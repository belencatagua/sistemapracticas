﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema
{
    class Nivel
    {
        private Carrera carrera;

        public Carrera Carrera
        {
            get { return carrera; }
            set { carrera = value; }
        }

        private int semestre;

        public int Semestre 
        {
            get { return semestre; }
            set { semestre  = value; }
        }

        private String nombreCarrera;

        public String NombreCarrera
        {
            get { return nombreCarrera; }
            set { nombreCarrera = value; }
        }


    }
}
