﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema
{
    class Estudiante
    {
        private Carrera carrera;

        public Carrera Carrera
        {
            get { return carrera; }
            set { carrera = value; }
        }


        private string Nombre;

        public string nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        private string Apellido;

        public string apellido
        {
            get { return Apellido; }
            set { Apellido = value; }
        }
        private string Cedula;

        public string cedula
        {
            get { return Cedula; }
            set { Cedula = value; }
        }
        private string  Nivel;

        public string  nivel
        {
            get { return Nivel; }
            set { Nivel = value; }
        }

    }
}
