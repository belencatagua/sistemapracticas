﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema
{
    class Documentos
    {
        private String id;

        public String Id 
        {
            get { return id; }
            set { id = value; }
        }

        private Nivel nivel;

        public Nivel Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }
        private String nombreDocumento;

        public String NombreDocumento
        {
            get { return nombreDocumento; }
            set { nombreDocumento = value; }
        }

    }
}
